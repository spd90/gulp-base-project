"use strict"

var gulp = require('gulp');
var browserify = require('browserify') // bundles JS
var babelify = require('babelify'); // transform ES6 to plain JS and ES5
var buffer = require('vinyl-buffer'); // creates a transform stream that takes vinyl files as input
var cleanCSS = require('gulp-clean-css'); // clean and minify css files
var concat = require('gulp-concat'); // concatenates files
var connect = require('gulp-connect'); // runs local dev server
var del = require('del');
var lint = require('gulp-eslint'); // lint JS files, including JSX
var open = require('gulp-open'); // open a URl in a web browser
var size = require('gulp-size'); // optimize size
var source = require('vinyl-source-stream'); // use conventional text streams with Gulp
var uglify = require('gulp-uglify'); // minify js files

var config = {
  port: 9005,
  devBaseUrl: 'http://localhost',
  paths: {
    html: './src/*.html',
    js: './src/**/*.js',
    css: [
      'node_modules/bootstrap/dist/css/bootstrap.min.css',
      'node_modules/bootstrap/dist/css/bootstrap-theme.min.css',
      './src/css/main.css'
    ],
    images: './src/images/*',
    dist: './dist',
    build: './build',
    indexJs: './src/index.js',
  }
}

// start local development server
gulp.task('connect', function() {
  connect.server({
    root: ['build'],
    port: config.port,
    base: config.devBaseUrl,
    livereload: true
  })
});

// open browser with uri
gulp.task('open', ['connect'], function() {
  gulp.src('build/index.html')
    .pipe(open({
      uri: config.devBaseUrl + ':' + config.port + '/',
      app: 'chrome' // we can also use firefox
    }));
});

// clean dist folder in first run to make everything up to date
gulp.task('clean:build', function(){
    return del('build/**', {force:true, readFile: false});
});

gulp.task('clean:dist', function(){
    return del('dist/**', {force:true, readFile: false});
});

gulp.task('html', function() {
  gulp.src(config.paths.html)
    .pipe(gulp.dest(config.paths.build))
    .pipe(connect.reload());
});

gulp.task('html:dist', function() {
  gulp.src(config.paths.html)
    .pipe(gulp.dest(config.paths.dist));
});

gulp.task('js', function() {
  browserify(config.paths.indexJs)
    .transform("babelify")
    .bundle()
    .on('error', console.error.bind(console))
    .pipe(source('bundle.js'))
    .pipe(gulp.dest(config.paths.build + '/scripts'))
    .pipe(connect.reload());
});

gulp.task('js:dist', function() {
  browserify(config.paths.indexJs)
    .transform("babelify")
    .bundle()
    .on('error', console.error.bind(console))
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(size())
    .pipe(gulp.dest(config.paths.dist + '/scripts'));
});

gulp.task('css', function() {
  gulp.src(config.paths.css)
    .pipe(concat('bundle.css'))
    .pipe(gulp.dest(config.paths.build + '/css'));
});

gulp.task('css:dist', function() {
  gulp.src(config.paths.css)
    .pipe(concat('bundle.css'))
    .pipe(buffer())
    .pipe(cleanCSS({debug: false}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
    .pipe(gulp.dest(config.paths.dist + '/css'));
});

gulp.task('images', function() {
  gulp.src(config.paths.images)
    .pipe(gulp.dest(config.paths.build + '/images'))
    .pipe(connect.reload());

    // publish favicon
    gulp.src('./src/favicon.ico')
    .pipe(gulp.dest(config.paths.build));
});

gulp.task('images:dist', function() {
  gulp.src(config.paths.images)
    .pipe(gulp.dest(config.paths.dist + '/images'));

    // publish favicon
    gulp.src('./src/favicon.ico')
    .pipe(gulp.dest(config.paths.dist));
});

gulp.task('lint', function() {
  return gulp.src(config.paths.js)
    .pipe(lint({
      configFile: '.eslintrc'
    }))
    .pipe(lint.format())
    .pipe(lint.failAfterError());
});

gulp.task('watch', function() {
  gulp.watch(config.paths.html, ['html']);
  gulp.watch(config.paths.css, ['css']);
  gulp.watch(config.paths.js, ['js', 'lint']);
});

gulp.task('default', ['clean:build', 'html', 'lint', 'js', 'css', 'images', 'open', 'watch']);
gulp.task('dist', ['clean:dist', 'html:dist', 'lint', 'js:dist', 'css:dist', 'images:dist']);
